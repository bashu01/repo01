import os
import datetime
from fabric.api import local, run, env, get

env.hosts = [
        '52.15.109.223',
        '18.218.17.17'
]

env.user = 'ubuntu'
s3_bucket = 'test-fabric'

databases = [
    {
    'user': 'root'
    }
]

time_tag = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
def pull_databases():
    global databases
    # List all databases
    for index, database in enumerate(databases):
        run("echo $MYSQL_PWD && mysql -u "+database['user']+" -p"+os.environ['MYSQL_PASSWORD']+" -e 'SHOW DATABASES;' | grep -Ev '^(Database|mysql|performance_schema|information_schema|sys)$' > databases.txt")
       # run("mysql -u "+database['user']+" -p"+os.environ['MYSQL_PASSWORD']+" -e 'SHOW DATABASES;' | tr -d '| ' | grep -v Database > ~/databases.txt")
    # backup & gzip all remote databases
    
    for index, database in enumerate(databases):
        get('databases.txt', 'databases.txt')
        with open('databases.txt') as db:
           for data in db:
               line = data.strip()
               run("mysqldump -u "+database['user']+" -p"+os.environ['MYSQL_PASSWORD']+" "+line+" | gzip -9 > "+line+"-"+time_tag+".sql.gz")
             # secure copy to local environment
               local("scp -o 'StrictHostKeyChecking no' -i /opt/atlassian/pipelines/agent/data/id_rsa "+env.user+"@"+env.host_string+":"+line+"-"+time_tag+".sql.gz .")
                # copy remote backups to s3 bucket
               local("aws s3 cp "+line+"-"+time_tag+".sql.gz s3://"+s3_bucket+"/"+env.host_string+"/")

            # cleanup files from remote environments
               run("rm "+line+"-"+time_tag+".sql.gz")
               run("echo 'job completed'")
               run("exit 0")
               